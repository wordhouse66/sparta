class MessengerMailer < ApplicationMailer
default from: 'enquiry@spartaincnigeria.com'
 
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.messenger_mailer.contact_mail.subject
  #
  def contact_mail(messenger)
  	@messenger = messenger
    #info@spartaincnigeria.com, spartaincnigeria@gmail.com, 

    mail( to: "info@spartaincnigeria.com", subject: @messenger.subject)
  end
end
