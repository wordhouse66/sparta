class Messenger < ApplicationRecord

	validates :name, presence: true
    validates :message, presence: true
    validates :subject, presence: true
    validates :email, presence: true



end
