json.extract! messenger, :id, :name, :email, :subject, :message, :created_at, :updated_at
json.url messenger_url(messenger, format: :json)
