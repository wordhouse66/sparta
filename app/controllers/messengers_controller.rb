class MessengersController < ApplicationController
  before_action :set_messenger, only: [:show, :edit, :update, :destroy]
  #before_action :authenticate_user!, only: [:index, :edit, :update, :destroy]
  # GET /messengers
  # GET /messengers.json
  

  # GET /messengers/new
  def new
    @messenger = Messenger.new
  end

 

  # POST /messengers
  # POST /messengers.json
  def create
    @messenger = Messenger.new(messenger_params)

    respond_to do |format|
      if @messenger.save
        MessengerMailer.contact_mail(@messenger).deliver_now
        format.html { redirect_to root_url, notice: 'Messsage was successfully sent.' }
        format.json { render :show, status: :created, location: root_url }
      else
        format.html { redirect_to root_url, notice: @messenger.errors}
        format.json { render json: @messenger.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /messengers/1
  # PATCH/PUT /messengers/1.json
  
  # DELETE /messengers/1
  # DELETE /messengers/1.json
  def destroy
    @messenger.destroy
    respond_to do |format|
      format.html { redirect_to messengers_url, notice: 'Messenger was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_messenger
      @messenger = Messenger.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def messenger_params
      params.require(:messenger).permit(:name, :email, :subject, :message)
    end
end
