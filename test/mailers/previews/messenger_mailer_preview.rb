# Preview all emails at http://localhost:3000/rails/mailers/messenger_mailer
class MessengerMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/messenger_mailer/contact_mail
  def contact_mail
    MessengerMailer.contact_mail
  end

end
